# Arduino reloj RTC DS3231 y display 4 Digitos

Un proyecto muy sencillo, utilizando un Arduino Nano, un reloj RTC DS3231 con conexión I2C y un Display de 4 dígitos (LCD de 7 segmentos)

Librerías:
https://github.com/adafruit/RTClib
https://github.com/avishorp/TM1637
